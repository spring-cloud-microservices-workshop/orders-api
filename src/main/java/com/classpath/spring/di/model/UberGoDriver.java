package com.classpath.spring.di.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class UberGoDriver implements UberCommute {

    @Value("ramesh")
    private String name;

    public UberGoDriver() {
    }

    public void commute(String fromLocation, String toLocation){
        System.out.println("Commuting from "+ fromLocation + " to location "+ toLocation + " with Uber Go !! with "+ this.name);
    }
}
package com.classpath.spring.di.client;

import com.classpath.spring.di.model.Customer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UberCabBooking {

    public static void main(String[] args) {
        //UberCommute ramesh = new UberPrimeDriver("Ramesh");
        //Customer vignesh = new Customer(ramesh);
        //vignesh.travel("RR Nagar", " BIAL");
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        Customer customer = applicationContext.getBean("customer", Customer.class);
        customer.travel("RR Nagar", " BIAL");


    }
}
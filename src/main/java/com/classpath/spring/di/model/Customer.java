package com.classpath.spring.di.model;

import org.springframework.stereotype.Component;

@Component
public class Customer {

    private final UberCommute uberCommute;

    public Customer(UberCommute uberDriver){
        this.uberCommute = uberDriver;
    }

    public void travel(String from, String to){
        this.uberCommute.commute(from, to);
    }
}
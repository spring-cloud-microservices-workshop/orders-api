package com.classpath.spring.di.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UberPrimeDriver implements UberCommute{

    @Value("Suresh")
    private  String name;

    public UberPrimeDriver() {
    }

    @Override
    public void commute(String fromLocation, String toLocation) {
        System.out.println("Commuting from "+ fromLocation + " to location "+ toLocation + " with Uber Prime !! with "+ this.name);

    }
}
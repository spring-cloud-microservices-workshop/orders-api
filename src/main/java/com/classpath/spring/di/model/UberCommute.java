package com.classpath.spring.di.model;

public interface UberCommute {

    void commute(String fromLocation, String toLocation);
}